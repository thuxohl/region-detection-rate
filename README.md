# Region Detection Rate

An implementation of the region detection rate as described in the paper.
The repository also contains code for common pixel-wise measures and for the measure used in the ICDAR competition.

## Usage
The implementation requires OpenCV an Numpy.
These requirements can be installed with `pip install -r requirements.txt`.
```
Usage: Compute measures for a prediction [-h] [--verbose] [--visualize] prediction annotation

positional arguments:
  prediction   the prediction for which the measures should be computed
  annotation   the annotation for the prediction

optional arguments:
  -h, --help   show this help message and exit
  --verbose    print lots of additional information (default: False)
  --visualize  visualize decisions made by the region detection rate (default: False)
```

For example:
`python -m rdr examples/laundry/prediction.png examples/laundry/annotation.png`

## Citation
If you use this code, please cite our Paper:

`T. Huxohl, and F. Kummert, "Region Detection Rate: An Applied Measure for Surface Defect Localization," 2021 IEEE International Conference on Signal and Image Processing Applications (ICSIPA), 2021.`

## References
* `C. Wolf and J.-M. Jolion, “Object count/area graphs for the evaluation of object detection and segmentation algorithms,” International Journal of Document Analysis and Recognition (IJDAR), vol. 8, no. 4, pp. 280–296, 2006.`

