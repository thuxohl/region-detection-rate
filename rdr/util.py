import cv2 as cv
import numpy as np

class BoundingBox():
    """
    Helper class for bounding boxes.
    """
    def __init__(self, left, top, width, height):
        """
        Initialize a bounding box with the top-left coordinate
        and its width and height.
        """
        self.left = left
        self.right = left + width

        self.top = top
        self.bottom = top + height

        self.width = width
        self.height = height

    def __str__(self):
        return str(self.left) + ', ' + str(self.top) + ' x ' + \
               str(self.width) + ', ' + str(self.height)

    def __repr__(self):
        return self.__str__()

    def slice(self):
        return (slice(self.top, self.bottom), slice(self.left, self.right))

    def union(self, other):
        """
        Create a union of two bounding boxes.
        """
        left = min(self.left, other.left)
        right = max(self.right, other.right)

        top = min(self.top, other.top)
        bottom = max(self.bottom, other.bottom)

        return BoundingBox(left, top, right - left, bottom - top)

    def intersection(self, other):
        """
        Create the intersection of two bounding boxes.

        raise: ValueError if the intersection is empty
        """
        left = max(self.left, other.left)
        right = min(self.right, other.right)

        top = max(self.top, other.top)
        bottom = min(self.bottom, other.bottom)

        if left >= right or top >= bottom:
            raise ValueError()

        return BoundingBox(left, top, right - left, bottom - top)

    def intersects(self, other):
        try:
            self.intersection(other)
            return True
        except:
            return False

    def tl(self):
        return np.array((self.top, self.left))

    def tr(self):
        return np.array((self.top, self.left + self.width))

    def bl(self):
        return np.array((self.top + self.height, self.left))

    def br(self):
        return np.array((self.top + self.height, self.left + self.width))

    def corner_points(self):
        return [self.tl(), self.tr(), self.br(), self.bl()]


class ConnectedComponent:

    def __init__(self, label, label_img, stats, centroid):
        self.label = label
        self.label_img = label_img == label
        self.stats = stats
        self.centroid = centroid
        self.bounding_box = BoundingBox(*self.stats[:4])
        self.area = self.stats[cv.CC_STAT_AREA]

    def intersection_count(self, other):
        bb = self.bounding_box
        bb_other = other.bounding_box

        if not bb.intersects(bb_other):
            return 0

        _slice = bb.union(bb_other).slice()
        return np.logical_and(self.label_img[_slice], other.label_img[_slice]).sum()

    def union_count(self, other):
        bb = self.bounding_box
        bb_other = other.bounding_box

        _slice = bb.union(bb_other).slice()
        return np.logical_or(self.label_img[_slice], other.label_img[_slice]).sum()


def compute_connected_components(img, connectivity=8):
    component_count, label_img, stats, centroids = cv.connectedComponentsWithStats(img, connectivity)

    components = []
    for label in range(1, component_count):
        components.append(ConnectedComponent(label, label_img, stats[label], centroids[label]))

    return components

def vis_pre(ccs_pre, valid_predictions):
    img = np.zeros((*ccs_pre[0].label_img.shape, 3), np.uint8)
    for cc_pre in ccs_pre:
        # color = (28, 136, 237) if cc_pre in valid_predictions else (255, 135, 26)
        color = (255, 135, 26) if cc_pre in valid_predictions else (28, 136, 237)
        color_img = np.full(img.shape, color, np.uint8)
       
        # img = np.where(cc_pre.label_img.repeat(3).reshape(img.shape), color_img, img)
        img = np.where(cc_pre.label_img.repeat(3).reshape(img.shape), color_img, img)
    return img

class PixelwiseQuantities:

    def __init__(self, tp: int = 0, tn: int = 0, fp: int = 0, fn: int = 0):
        self.tp = tp
        self.tn = tn
        self.fp = fp
        self.fn = fn

    def __add__(self, other):
        tp = self.tp + other.tp
        tn = self.tn + other.tn
        fp = self.fp + other.fp
        fn = self.fn + other.fn
        return PixelwiseQuantities(tp, tn, fp, fn)

    def __repr__(self):
        l = [self.tp, self.tn, self.fp, self.fn]
        max_len = len(str(max([self.tp, self.tn, self.fp, self.fn])))
        s = sum(l)
        ls = [x / s for x in l]
        return f'[TP, FP] = [{self.tp:{max_len}d}, {self.fp:{max_len}d}] = [{ls[0]:.3f}, {ls[2]:.3f}]\n' + \
               f'[TN, FN] = [{self.tn:{max_len}d}, {self.fn:{max_len}d}] = [{ls[1]:.3f}, {ls[3]:.3f}]'

    @classmethod
    def compute(cls, prediction: np.array, annotation: np.array):
        prediction = prediction > 0
        annotation = annotation > 0

        intersection = np.logical_and(prediction, annotation)

        tp = intersection.sum()

        fp = prediction.sum() - tp
        fn = annotation.sum() - tp

        pixel_count = prediction.shape[0] * prediction.shape[1]
        tn = pixel_count - tp - fp - fn

        return cls(tp, tn, fp, fn)
