import cv2 as cv
import numpy as np

from rdr.util import PixelwiseQuantities


def precision(q: PixelwiseQuantities):
    return q.tp / (q.tp + q.fp)


def recall(q: PixelwiseQuantities):
    return q.tp / (q.tp + q.fn)


def iou(q: PixelwiseQuantities):
    denominator = q.tp + q.fp + q.fn
    return q.tp / denominator


def f_beta(precision: float, recall: float, beta_squared: float=1.0):
    return ((1 + beta_squared) * precision * recall) / (beta_squared * precision + recall) 


def icdar(ccs_ann, ccs_pre, t_r=0.5, t_p=0.2, detected_regions=None, valid_predictions=None, verbose=False):
    """
    Implementation of the measure used in the ICDAR competition as proposed by
    Wold and Jolion in "Object count/area graphs for the evaluation of object detection
    and segmentation algorithms", 2006.
    """
    if detected_regions is None:
        detected_regions = set()
    if valid_predictions is None:
        valid_predictions = set()

    if verbose:
        print("Compute ICDAR measure...")

    tau   = np.zeros((len(ccs_ann), len(ccs_pre)))
    sigma = np.zeros(tau.shape)
    for i, cc_ann in enumerate(ccs_ann):
        for j, cc_pre in enumerate(ccs_pre):
            intersection_count = cc_ann.intersection_count(cc_pre)
            sigma[i, j] = intersection_count / cc_ann.area
            tau[i, j]   = intersection_count / cc_pre.area


    for i, cc_ann in enumerate(ccs_ann):
        if sum(sigma[i][tau[i] >= t_p]) >= t_r:
            detected_regions.add(cc_ann)

    for j, cc_pre in enumerate(ccs_pre):
        if sum(tau[:, j][sigma[:, j] >= t_r]) >= t_p:
            valid_predictions.add(cc_pre)


    recall = len(detected_regions) / len(ccs_ann)
    precision = len(valid_predictions) / len(ccs_pre)
 
    if verbose:
        print(f"Detected Regions: {list(map(lambda cc: cc.label, detected_regions))}")
        print(f"Valid Predictions: {list(map(lambda cc: cc.label, valid_predictions))}")
        print(f"Recall: {recall:.4f}")
        print(f"Precision: {precision:.4f}")

    return f_beta(precision, recall, beta_squared=1.0)


def rdr(ccs_ann, ccs_pre, t_r=0.5, t_p=0.2, alpha=0.5, detected_regions=None, valid_predictions=None, verbose=False):
    """
    Implementation of the region detection rate.
    """
    if detected_regions is None:
        detected_regions = set()
    if valid_predictions is None:
        valid_predictions = set()

    if verbose:
        print("Compute RDR measure...")

    tau   = np.zeros((len(ccs_ann), len(ccs_pre)))
    sigma = np.zeros(tau.shape)
    for i, cc_ann in enumerate(ccs_ann):
        for j, cc_pre in enumerate(ccs_pre):
            intersection_count = cc_ann.intersection_count(cc_pre)

            tau[i, j]   = intersection_count / cc_pre.area
            sigma[i, j] = intersection_count / cc_ann.area

    for i, cc_ann in enumerate(ccs_ann):
        if sum(sigma[i][tau[i] >= t_p]) >= t_r:
            detected_regions.add(cc_ann)

    for j, cc_pre in enumerate(ccs_pre):
        if (tau[:, j] >= t_p).any():
            valid_predictions.add(cc_pre)

    recall = len(detected_regions) / len(ccs_ann)
    precision = len(valid_predictions) / len(ccs_pre)

    if verbose:
        print(f"Detected Regions: {list(map(lambda cc: cc.label, detected_regions))}")
        print(f"Valid Predictions: {list(map(lambda cc: cc.label, valid_predictions))}")
        print(f"Recall: {recall:.4f}")
        print(f"Precision: {precision:.4f}")
        print(f"F1 {f_beta(precision, recall, beta_squared=2.0)}")
        print(f"F2 {f_beta(precision, recall, beta_squared=1.0)}")

    false_prediction_count = len(ccs_pre) - len(valid_predictions)
    return len(detected_regions) / (len(ccs_ann) + alpha * false_prediction_count)
