import argparse

import cv2 as cv
import numpy as np

import rdr.measures as m
from rdr.util import compute_connected_components, PixelwiseQuantities, vis_pre


parser = argparse.ArgumentParser("Compute measures for a prediction",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("prediction", type=str,
                    help="the prediction for which the measures should be computed")
parser.add_argument("annotation", type=str,
                    help="the annotation for the prediction")
parser.add_argument("--verbose", action="store_true",
                    help="print lots of additional information")
parser.add_argument("--visualize", action="store_true",
                    help="visualize decisions made by the region detection rate")
args = parser.parse_args()


def print_meaures(name_value_dict, elem_len=10, value_precision=4):
    name_strs = []
    val_strs = []
    for name, value in name_value_dict.items():
        val_str = f"{value:.{value_precision}f}"
        val_strs.append(f"{val_str:>{elem_len}}")

        name_strs.append(f"{name:>{elem_len}}")
    print(" ".join(name_strs))
    print(" ".join(val_strs))


pre = cv.imread(args.prediction, cv.IMREAD_GRAYSCALE)
ann = cv.imread(args.annotation, cv.IMREAD_GRAYSCALE)

pq = PixelwiseQuantities.compute(pre, ann)

print("Pixelwise measures:")
print()
if (args.verbose):
    print("Quantities:")
    print(pq)
    print()

precision = m.precision(pq)
recall = m.recall(pq)
iou = m.iou(pq)
f_beta = m.f_beta(precision, recall)

print_meaures({"Precision": precision,
               "Recall": recall,
               "IoU": iou,
               "FBeta": f_beta})
print()

print("Instance measures:")
print()

ccs_ann = compute_connected_components(ann)
ccs_pre = compute_connected_components(pre)

if args.verbose:
    print(f"Predicted regions {len(ccs_pre)}")
    print(f"Annotated regions {len(ccs_ann)}")
    print()

detected_regions_rdr = set()
valid_predictions_rdr = set()
rdr = m.rdr(ccs_ann, ccs_pre, verbose=args.verbose, valid_predictions=valid_predictions_rdr, detected_regions=detected_regions_rdr)

if args.verbose:
    print()
icdar = m.icdar(ccs_ann, ccs_pre)
if args.verbose:
    print()
print_meaures({"RDR": rdr,
               "ICDAR": icdar})
print()

if args.visualize:
    cv.namedWindow("RDR", cv.WINDOW_NORMAL)
    cv.resizeWindow("RDR", 1600, 900)

    img_pre_rdr = vis_pre(ccs_pre, valid_predictions_rdr)
    img_ann_rdr = vis_pre(ccs_ann, detected_regions_rdr)
    filler = np.full((img_pre_rdr.shape[0], 20, 3), 255, np.uint8)
    img_rdr = np.hstack((img_pre_rdr, filler, img_ann_rdr))

    cv.imshow("RDR", img_rdr)
    
    if cv.waitKey(0) == ord("s"):
        print("Save images...")
        cv.imwrite("rdr_pre.png", img_pre_rdr)
        cv.imwrite("rdr_ann.png", img_ann_rdr)
